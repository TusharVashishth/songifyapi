const mongoose = require("mongoose")
const Schema = mongoose.Schema

const AdminDashBoard = new Schema({
  heading: {
    type: String
  },
  text: {
    type: String
  },
  image: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model("admin_dashboard", AdminDashBoard)
