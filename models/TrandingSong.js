const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrandingSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    length: {
        type: String,
        required: true
    },
    artist: {
        type: String,
        required: true
    },
    genre: {
        type: String,
        required: true
    },
    mood: {
        type: String,
        required: true
    },
    song: {
        type: String,
        required: true
    },
    image: {
        type: String,
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = Tranding = mongoose.model('tranding_songs', TrandingSchema);