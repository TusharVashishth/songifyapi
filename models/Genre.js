const mongoose = require("mongoose")
const Schema = mongoose.Schema

const GenreSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  imagePath: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
})

module.exports = Genre = mongoose.model("genres", GenreSchema)
