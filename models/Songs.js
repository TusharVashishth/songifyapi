const mongoose = require("mongoose")
const Schema = mongoose.Schema
const SongSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  length: {
    type: String,
    required: true
  },
  artist: {
    type: String,
    required: true
  },
  genre: {
    type: String,
    required: true
  },
  mood: {
    type: String,
    required: true
  },
  release: {
    type: String
  },
  tranding: {
    type: String
  },
  actor: {
    type: String
  },
  song: {
    type: String,
    required: true
  },
  songPath: {
    type: String
  },
  imagePath: {
    type: String
  },
  image: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
})

module.exports = Song = mongoose.model("songs", SongSchema)
