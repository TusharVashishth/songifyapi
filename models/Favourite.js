const mongoose = require("mongoose")
const Schema = mongoose.Schema

const FavouriteSong = new Schema({
  user_id: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  song: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  songPath: {
    type: String
  },
  imagePath: {
    type: String
  },
  date: {
    type: String,
    default: Date.now
  }
})

module.exports = Favourite = mongoose.model("favouriteSongs", FavouriteSong)
