const mongoose = require("mongoose")
const Schema = mongoose.Schema

const ArtistSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  imagePath: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
})

module.exports = Artist = mongoose.model("artists", ArtistSchema)
