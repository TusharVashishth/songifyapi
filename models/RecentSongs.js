const mongoose = require("mongoose")
const Schema = mongoose.Schema
const recentSchema = new Schema({
  user_id: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  count: {
    type: Number,
    default: 0
  },
  song: {
    type: String,
    required: true
  },
  songPath: {
    type: String
  },
  imagePath: {
    type: String
  },
  image: {
    type: String,
    required: true
  },
  date: {
    type: String,
    default: Date.now
  }
})

module.exports = recentSongs = mongoose.model("recentSongs", recentSchema)
