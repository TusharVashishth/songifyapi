const express = require("express")
const router = express.Router()
const Artist = require("../../../models/Artist")
const passport = require("passport")
const fileUpload = require("express-fileupload")
const artistValidation = require("../../../validation/artistValidation")
const isEmpty = require("../../../validation/isEmpty")
const fs = require("fs")
router.use(fileUpload())

// @method POST
// @url /api/admin/add_artist
// @desc Private

router.get(
  "/",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    Artist.find()
      .then((artist) => {
        res.json(artist)
      })
      .catch((err) => console.log(err))
  }
)

// @method POST
// @url /api/admin/add_artist
// @desc Private

router.post(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const { isValid, errors } = artistValidation(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    if (isEmpty(req.files)) {
      return res.status(400).json({ image: "Image Field is Required" })
    }
    const file = req.files.image
    // Check Image Type
    type = ["image/jpeg", "image/png", "image/jpg", "image/jifi"]
    getData = type.indexOf(file.mimetype)
    if (getData === -1) {
      return res.status(400).json({ image: "File must be JPG,JPEG,PNG,JIFI" })
    }
    // Check File Size
    if (file.size > 1000000) {
      return res.status(400).json({ image: "Image size must be below 1 Mb" })
    }
    const fileName = Date.now() + "_" + file.name
    file.mv(`public/ArtistImages/${fileName}`, (err) => {
      if (err) {
        console.log(err)
      }
    })

    const artistObject = {}
    if (req.body.name) artistObject.name = req.body.name
    if (req.files.image) artistObject.image = fileName
    artistObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/ArtistImages/${fileName}`
    Artist.findOne({ name: req.body.name })
      .then((artist) => {
        if (artist) {
          const path = `public/ArtistImages/${artist.image}`
          if (artist.image) {
            fs.unlink(path, (err) => {
              if (err) {
                console.log(err)
              }
            })
          }

          Artist.findByIdAndUpdate(
            { _id: artist._id },
            { $set: artistObject },
            { new: true }
          )
            .then((artist) => res.json(artist))
            .catch((err) => console.log(err))
        } else {
          const newArtist = new Artist(artistObject)
          newArtist
            .save()
            .then((artist) => res.json(artist))
            .catch((err) => console.log(err))
        }
      })
      .catch((err) => console.log(err))
  }
)

// @method DELETE
// @url /api/admin/add_artist
// @desc Private

router.delete(
  "/:id",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const id = req.params.id
    Artist.findByIdAndRemove({ _id: id })
      .then((artist) => {
        const path = `public/ArtistImages/${artist.image}`
        fs.unlink(path, (err) => {
          if (err) {
            console.log(err)
          }
        })
        res.json(artist)
      })
      .catch((err) => console.log(err))
  }
)
module.exports = router
