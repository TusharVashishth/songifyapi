const express = require("express")
const router = express.Router()
const recentSong = require("../../../models/RecentSongs")
const fileUpload = require("express-fileupload")
const passport = require("passport")
router.use(fileUpload())

// @POST Method GET
// @url /api/admin/recent/:user_id
// @desc Private Route

router.get("/:user_id", (req, res) => {
  recentSong
    .find({ user_id: req.params.user_id })
    .sort({ date: -1 })
    .then((songs) => res.json(songs))
    .catch((err) => console.log(err))
})

// @POST Method GET
// @url /api/admin/recent/mostplayed/:user_id
// @desc Private Route

router.get("/mostplayed/:user_id", (req, res) => {
  recentSong
    .find({ user_id: req.params.user_id, count: { $gte: 3 } })
    .sort({ date: -1 })
    .then((songs) => res.json(songs))
    .catch((err) => console.log(err))
})

// @POST Method POST
// @url /api/admin/recent
// @desc Private Route
router.post("/", (req, res) => {
  const songObject = {}
  if (req.body.user_id) songObject.user_id = req.body.user_id
  if (req.body.title) songObject.title = req.body.title
  if (req.body.description) songObject.description = req.body.description
  if (req.body.song) songObject.song = req.body.song
  if (req.body.image) songObject.image = req.body.image
  songObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/Images/${req.body.image}`
  songObject.songPath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/Songs/${req.body.song}`
  recentSong
    .findOne({ song: songObject.song, user_id: songObject.user_id })
    .then((song) => {
      if (song) {
        recentSong
          .findOneAndUpdate(
            { _id: song._id },
            { $set: { count: song.count + 1 } },
            { $new: true }
          )
          .then(() => console.log(" "))
          .catch((err) => console.log(err))
      } else {
        const newSong = new recentSong(songObject)
        newSong
          .save()
          .then((song) => res.json(song))
          .catch((err) => console.log(err))
      }
    })
    .catch((err) => console.log(err))
})
module.exports = router
