const express = require("express");
const router = express.Router();
const passport = require("passport");
const User = require("../../../models/User");

// Get User List
// @Route /api/admin/auth-users
// @Private Route
// @Method Get

router.get(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    User.find()
      .then(users => {
        return res.json(users);
      })
      .catch(err => console.log(err));
  }
);
module.exports = router;
