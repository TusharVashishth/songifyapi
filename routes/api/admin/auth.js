const express = require('express');
const router = express.Router();
const Admin = require('../../../models/AdminAuth');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const Key = require('../../../config/keys');
const registerValidation = require('../../../validation/registerValidation');
const loginValidation = require('../../../validation/loginValidation');
// @Method POST
// @url /api/admin/register
// @desc Public

router.post('/register' , (req,res) => {
    const {isValid , errors} = registerValidation(req.body);
    if(!isValid) {
        return res.status(400).json(errors);
    }
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    Admin.findOne({email:email})
    .then(admin => {
        if(admin) {
            return res.status(400).json({email:"Email Already Exist in Our DataBase"})
        }
        const newAdmin = new Admin({
            name,email,password
        });
        bcrypt.genSalt(10 , (err , salt) => {
            bcrypt.hash(newAdmin.password , salt , (err , hash) => {
                if(err) throw err;
                newAdmin.password = hash;
                newAdmin
                    .save()
                    .then(admin => res.json(admin))
                    .catch(err => console.log(err))
            })
        })  
    })
    .catch(err => console.log(err))
});

// @Method POST
// @url /api/admin/login
// @desc Public

router.post('/login' , (req,res) => {
    const {isValid , errors} = loginValidation(req.body);
    if(!isValid) {
        return res.status(400).json(errors);
    }
    const email = req.body.email;
    const password = req.body.password;

    Admin.findOne({email:email})
            .then( admin => {
                if(!admin) {
                    return res.status(404).json({email:"No Email Found In Our DataBase"})
                }
                bcrypt.compare(password , admin.password)
                .then(isMatch => {
                    if(isMatch) {
                       const paylod = {
                           id:admin.id,
                           name:admin.name,
                           email:admin.email
                       }
                       const secret = Key.secretKey;
                       jwt.sign(
                           paylod,
                           secret,
                           {expiresIn : '3600h'},
                           (err , token) => {
                               if(token) {
                                   res.json({
                                       success:true,
                                       token:"Bearer " +token
                                   })
                               }

                           }
                       )
                    }
                    else {
                        return res.status(400).json({password:"password Not Match"});
                    }
                })
                .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
})
module.exports = router;