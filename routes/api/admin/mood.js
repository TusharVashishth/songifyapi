const express = require("express")
const router = express.Router()
const fileUpload = require("express-fileupload")
const passport = require("passport")
const Mood = require("../../../models/Mood")
const moodValidation = require("../../../validation/moodValidation")
const fs = require("fs")

router.use(fileUpload())

// @method GET
// @url /api/admin/mood
// @desc Private Route

router.get("/", (req, res) => {
  Mood.find()
    .then((mood) => res.json(mood))
    .catch((err) => console.log(err))
})
// @method POST
//  @url /api/admin/mood
// @desc Private Route

router.post(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const { isValid, errors } = moodValidation(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }
    const file = req.files.image
    const fileName = Date.now() + "_" + file.name
    file.mv(`public/GenreMoods/${fileName}`, (err) => {
      if (err) {
        console.log(err)
      }
    })
    const moodObject = {}
    if (req.body.type) moodObject.type = req.body.type
    if (req.files.image) moodObject.image = fileName
    moodObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/GenreMoods/${fileName}`

    Mood.findOne({ type: req.body.type })
      .then((mood) => {
        if (mood) {
          const path = `public/GenreMoods/${mood.image}`
          if (mood.image) {
            fs.unlink(path, (err) => {
              if (err) {
                console.log(err)
              }
            })
          }

          Mood.findOneAndUpdate(
            { _id: mood._id },
            { $set: moodObject },
            { new: true }
          )
            .then((mood) => res.json(mood))
            .catch((err) => console.log(err))
        } else {
          const newMood = new Mood(moodObject)
          newMood
            .save()
            .then((mood) => res.json(mood))
            .catch((err) => console.log(err))
        }
      })
      .catch((err) => console.log(err))
  }
)

// @method DELETE
//  @url /api/admin/mood
// @desc Private Route
router.delete(
  "/:id",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    Mood.findOneAndDelete({ _id: req.params.id })
      .then((mood) => {
        const path = `public/GenreMoods/${mood.image}`
        if (mood.image) {
          fs.unlink(path, (err) => {
            if (err) {
              console.log(err)
            }
          })
        }

        res.json(mood)
      })
      .catch((err) => console.log(err))
  }
)

module.exports = router
