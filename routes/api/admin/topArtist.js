const express = require("express")
const router = express.Router()
const TopArtist = require("../../../models/TopArtist")
const passport = require("passport")
const fileUpload = require("express-fileupload")
const artistValidation = require("../../../validation/artistValidation")
const fs = require("fs")
router.use(fileUpload())

// @method Get
// @url /api/admin/top_artist
// @desc Public

router.get("/", (req, res) => {
  TopArtist.find()
    .then((artist) => {
      res.json(artist)
    })
    .catch((err) => console.log(err))
})

// @method POST
// @url /api/admin/top_artist
// @desc Private

router.post(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const { isValid, errors } = artistValidation(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }
    const file = req.files.image
    const fileName = Date.now() + "_" + file.name
    file.mv(`public/ArtistImages/${fileName}`, (err) => {
      if (err) {
        console.log(err)
      }
    })

    const artistObject = {}
    if (req.body.name) artistObject.name = req.body.name
    if (req.files.image) artistObject.image = fileName
    artistObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/ArtistImages/${fileName}`
    TopArtist.findOne({ name: req.body.name })
      .then((artist) => {
        if (artist) {
          const path = `public/ArtistImages/${artist.image}`
          if (artist.image) {
            fs.unlink(path, (err) => {
              if (err) {
                console.log(err)
              }
            })
          }

          TopArtist.findByIdAndUpdate(
            { _id: artist._id },
            { $set: artistObject },
            { new: true }
          )
            .then((artist) => res.json(artist))
            .catch((err) => console.log(err))
        } else {
          const newArtist = new TopArtist(artistObject)
          newArtist
            .save()
            .then((artist) => res.json(artist))
            .catch((err) => console.log(err))
        }
      })
      .catch((err) => console.log(err))
  }
)

// @method DELETE
// @url /api/admin/top_artist
// @desc Private

router.delete(
  "/:id",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const id = req.params.id
    TopArtist.findByIdAndRemove({ _id: id })
      .then((artist) => {
        const path = `public/ArtistImages/${artist.image}`
        fs.unlink(path, (err) => {
          if (err) {
            console.log(err)
          }
        })
        res.json(artist)
      })
      .catch((err) => console.log(err))
  }
)
module.exports = router
