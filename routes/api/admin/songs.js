const express = require("express")
const router = express.Router()
const Songs = require("../../../models/Songs")
const passport = require("passport")
const fileUpload = require("express-fileupload")
const songValidation = require("../../../validation/songValidation")
const isEmpty = require("../../../validation/isEmpty")
const fs = require("fs")
router.use(fileUpload())

// @POST Method GET
// @url /api/admin/song
// @desc Private Route

router.get(
  "/",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    Songs.find()
      .sort({ date: -1 })
      .then((songs) => res.json(songs))
      .catch((err) => console.log(err))
  }
)

// @POST Method GET
// @url /api/admin/song/random
// @desc Private Route To Get Random Song from dataBase

router.get(
  "/random",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    Songs.aggregate([{ $sample: { size: 100 } }])
      .then((songs) => res.json(songs))
      .catch((err) => console.log(err))
  }
)

// @POST Method GET
// @url /api/admin/song/new
// @desc Private Route

router.get(
  "/new",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    Songs.find({ release: "new" })
      .sort({ date: -1 })
      .then((songs) => res.json(songs))
      .catch((err) => console.log(err))
  }
)

// @POST Method GET
// @url /api/admin/song/tranding
// @desc Private Route

router.get(
  "/tranding",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    Songs.find({ tranding: "yes" })
      .sort({ date: -1 })
      .then((songs) => res.json(songs))
      .catch((err) => console.log(err))
  }
)

// @POST Method GET
// @url /api/admin/song/old
// @desc Private Route

router.get(
  "/old",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    Songs.find({ release: "old" })
      .sort({ date: -1 })
      .then((songs) => res.json(songs))
      .catch((err) => console.log(err))
  }
)

// @POST Method POST
// @url /api/admin/song
// @desc Private Route
router.post(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const { isValid, errors } = songValidation(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }
    // Check Song Type
    const song = req.files.song
    if (isEmpty(song)) {
      return res.status(400).json({ song: "Field is Required" })
    }

    songType = ["audio/mpeg", "audio/mp3"]
    getRecord = songType.indexOf(song.mimetype)
    if (getRecord === -1) {
      return res.status(400).json({ song: "File must be mp3" })
    }
    // For Image File
    const file = req.files.image
    if (isEmpty(file)) {
      // return res.status(400).json({ image: "Image Field is Required" })
      song.mv(`public/Songs/${song.name}`, (err) => {
        if (err) {
          console.log(err)
        }
      })
      const songObject = {}
      if (req.body.title) songObject.title = req.body.title
      if (req.body.description) songObject.description = req.body.description
      if (req.body.length) songObject.length = req.body.length
      if (req.body.artist) songObject.artist = req.body.artist
      if (req.body.genre) songObject.genre = req.body.genre
      if (req.body.mood) songObject.mood = req.body.mood
      if (req.body.actor) songObject.actor = req.body.actor
      if (req.body.release) songObject.release = req.body.release
      if (req.body.tranding) songObject.tranding = req.body.tranding
      if (req.files.song) songObject.song = song.name
      songObject.songPath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/${song.name}`
      songObject.imagePath =
        "https://a10.gaanacdn.com/images/gaanawebsite/albumdefaultcommonv1.jpg"

      Songs.findOne({ song: songObject.song })
        .then((song) => {
          if (song) {
            // const songPath = `Uploads/Songs/${song.song}`
            // fs.unlink(songPath, (err) => {
            //   if (err) {
            //     console.log(err)
            //   }
            // })
            Songs.findByIdAndUpdate(
              { _id: song._id },
              { $set: songObject },
              { new: true }
            )
              .then((song) => res.json(song))
              .catch((err) => console.log(err))
          } else {
            const newSong = new Songs(songObject)
            newSong
              .save()
              .then((song) => res.json(song))
              .catch((err) => console.log(err))
          }
        })
        .catch((err) => console.log(err))
    } else {
      // Check Image Type
      type = ["image/jpeg", "image/png", "image/jpg", "image/jifi"]
      getData = type.indexOf(file.mimetype)
      if (getData === -1) {
        return res.status(400).json({ image: "File must be JPG,JPEG,PNG,JIFI" })
      }
      // Check File Size
      if (file.size > 1000000) {
        return res.status(400).json({ image: "Image size must be below 1 Mb" })
      }
      const fileName = Date.now() + "_" + file.name
      song.mv(`public/Songs/${song.name}`, (err) => {
        if (err) {
          console.log(err)
        }
      })
      file.mv(`public/Images/${fileName}`, (err) => {
        if (err) {
          console.log(err)
        }
      })

      const songObject = {}
      if (req.body.title) songObject.title = req.body.title
      if (req.body.description) songObject.description = req.body.description
      if (req.body.length) songObject.length = req.body.length
      if (req.body.artist) songObject.artist = req.body.artist
      if (req.body.genre) songObject.genre = req.body.genre
      if (req.body.mood) songObject.mood = req.body.mood
      if (req.body.actor) songObject.actor = req.body.actor
      if (req.body.release) songObject.release = req.body.release
      if (req.body.tranding) songObject.tranding = req.body.tranding
      if (req.files.song) songObject.song = song.name
      if (req.files.image) songObject.image = fileName
      songObject.songPath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/Songs/${song.name}`
      songObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/Images/${fileName}`
      Songs.findOne({ song: songObject.song })
        .then((song) => {
          if (song) {
            // const songPath = `Uploads/Songs/${song.song}`
            // fs.unlink(songPath, (err) => {
            //   if (err) {
            //     console.log(err)
            //   }
            // })
            const imagePath = `public/Images/${song.image}`
            if (song.image) {
              fs.unlink(imagePath, (err) => {
                if (err) {
                  console.log(err)
                }
              })
            }

            Songs.findByIdAndUpdate(
              { _id: song._id },
              { $set: songObject },
              { new: true }
            )
              .then((song) => res.json(song))
              .catch((err) => console.log(err))
          } else {
            const newSong = new Songs(songObject)
            newSong
              .save()
              .then((song) => res.json(song))
              .catch((err) => console.log(err))
          }
        })
        .catch((err) => console.log(err))
    }
  }
)

// @POST Method POST
// @url api/admin/song/artist/artist_name
// @desc Private Route

router.post(
  "/artist/:artist",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    const artist = req.params.artist
    Songs.find({ artist: { $regex: ".*" + artist + ".*", $options: "<i>" } })
      .then((artist) => {
        if (artist.length > 0) {
          return res.json(artist)
        } else {
          return res.status(404).json({ search: "No Record Found" })
        }
      })
      .catch((err) => console.log(err))
  }
)

// @POST Method POST
// @url api/admin/song/song_name
// @desc Private Route

router.post(
  "/song_name/:name",
  // passport.authenticate("userStrategy", { session: false }),
  (req, res) => {
    let query = {}
    if (req.param.name) {
      query = {
        $or: [
          { song: { $regex: req.params.name, $options: "i" } },
          { artist: { $regex: req.params.name, $options: "i" } }
        ]
      }
    }
    // const songName = req.params.name;
    Songs.find(query)
      .then((songName) => {
        if (songName.length > 0) {
          return res.json(songName)
        } else {
          return res.status(404).json({ search: "No Record Found" })
        }
      })
      .catch((err) => console.log(err))
  }
)

// @POST Method POST
// @url /api/admin/song/mood/type
// @desc Private Route
router.post("/mood/:type", (req, res) => {
  Songs.find({ mood: { $regex: req.params.type, $options: "i" } })
    .then((songs) => res.json(songs))
    .catch((err) => console.log(err))
})

// @POST Method POST
// @url /api/admin/song/genre/type
// @desc Private Route
router.post("/genre/:type", (req, res) => {
  Songs.find({ genre: { $regex: req.params.type, $options: "i" } })
    .then((songs) => res.json(songs))
    .catch((err) => console.log(err))
})

// @POST Method DELETE
// @url /api/admin/song/
// @desc Private Route

router.delete(
  "/:id",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    Songs.findOneAndDelete({ _id: req.params.id })
      .then((song) => {
        const songPath = `public/Songs/${song.song}`
        fs.unlink(songPath, (err) => {
          if (err) {
            console.log(err)
          }
        })
        const imagePath = `public/Images/${song.image}`
        fs.unlink(imagePath, (err) => {
          if (err) {
            console.log(err)
          }
        })
        res.json(song)
      })
      .catch((err) => console.log(err))
  }
)
module.exports = router
