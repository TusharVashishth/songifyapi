const express = require("express")
const router = express.Router()
const AdminDashBoard = require("../../../models/AdminDashboard")
const passport = require("passport")
const fileUpload = require("express-fileupload")
const adminDashboard = require("../../../validation/admindashboardValidation")
const isEmpty = require("../../../validation/isEmpty")
const fs = require("fs")
router.use(fileUpload())

// @method POST
// @url /api/admin/dashboard
// @desc Private

router.get(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    AdminDashBoard.find()
      .then((dashboard) => {
        res.json(dashboard)
      })
      .catch((err) => console.log(err))
  }
)

// @method POST
// @url /api/admin/dashboard
// @desc Private

router.post(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const { isValid, errors } = adminDashboard(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }

    if (isEmpty(req.files)) {
      return res.status(400).json({ image: "Image Field is Required" })
    }
    const file = req.files.image
    // Check Image Type
    type = ["image/jpeg", "image/png", "image/jpg", "image/jifi"]
    getData = type.indexOf(file.mimetype)
    if (getData === -1) {
      return res.status(400).json({ image: "File must be JPG,JPEG,PNG,JIFI" })
    }
    // Check File Size
    if (file.size > 1000000) {
      return res.status(400).json({ image: "Image size must be below 1 Mb" })
    }
    const fileName = Date.now() + "_" + file.name
    file.mv(`public/Images/${fileName}`, (err) => {
      if (err) {
        console.log(err)
      }
    })

    const dashboardObject = {}
    if (req.body.heading) dashboardObject.heading = req.body.heading
    if (req.body.text) dashboardObject.text = req.body.text
    if (req.files.image) dashboardObject.image = fileName
    AdminDashBoard.findOne({ heading: req.body.heading })
      .then((heading) => {
        if (heading) {
          const path = `./client/public/UploadedImage/${heading.image}`
          fs.unlink(path, (err) => {
            if (err) {
              console.log(err)
            }
          })
          AdminDashBoard.findByIdAndUpdate(
            { _id: heading._id },
            { $set: dashboardObject },
            { new: true }
          )
            .then((heading) => res.json(heading))
            .catch((err) => console.log(err))
        } else {
          const newHeading = new AdminDashBoard(dashboardObject)
          newHeading
            .save()
            .then((heading) => res.json(heading))
            .catch((err) => console.log(err))
        }
      })
      .catch((err) => console.log(err))
  }
)

// @method DELETE
// @url /api/admin/dashboard
// @desc Private

router.delete(
  "/:id",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const id = req.params.id
    AdminDashBoard.findByIdAndRemove({ _id: id })
      .then((artist) => {
        const path = `./client/public/UploadedImage/${artist.image}`
        fs.unlink(path, (err) => {
          if (err) {
            console.log(err)
          }
        })
        res.json(artist)
      })
      .catch((err) => console.log(err))
  }
)
module.exports = router
