const express = require("express")
const router = express.Router()
const fileUpload = require("express-fileupload")
const passport = require("passport")
const Genre = require("../../../models/Genre")
const genreValidation = require("../../../validation/genreValidation")
const fs = require("fs")

router.use(fileUpload())

// @method GET
// @url /api/admin/genre
// @desc Private Route

router.get("/", (req, res) => {
  Genre.find()
    .then((genre) => res.json(genre))
    .catch((err) => console.log(err))
})
// @method POST
//  @url /api/admin/genre
// @desc Private Route

router.post(
  "/",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    const { isValid, errors } = genreValidation(req.body)
    if (!isValid) {
      return res.status(400).json(errors)
    }
    const file = req.files.image
    const fileName = Date.now() + "_" + file.name
    file.mv(`public/GenreMoods/${fileName}`, (err) => {
      if (err) {
        console.log(err)
      }
    })
    const genreObject = {}
    if (req.body.type) genreObject.type = req.body.type
    if (req.files.image) genreObject.image = fileName
    genreObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/GenreMoods/${fileName}`

    Genre.findOne({ type: req.body.type })
      .then((genre) => {
        if (genre) {
          const path = `public/GenreMoods/${genre.image}`
          if (genre.image) {
            fs.unlink(path, (err) => {
              if (err) {
                console.log(err)
              }
            })
          }

          Genre.findOneAndUpdate(
            { _id: genre._id },
            { $set: genreObject },
            { new: true }
          )
            .then((genre) => res.json(genre))
            .catch((err) => console.log(err))
        } else {
          const newGenre = new Genre(genreObject)
          newGenre
            .save()
            .then((genre) => res.json(genre))
            .catch((err) => console.log(err))
        }
      })
      .catch((err) => console.log(err))
  }
)

// @method DELETE
//  @url /api/admin/genre
// @desc Private Route
router.delete(
  "/:id",
  passport.authenticate("adminStrategy", { session: false }),
  (req, res) => {
    Genre.findOneAndDelete({ _id: req.params.id })
      .then((genre) => {
        const path = `public/GenreMoods/${genre.image}`
        fs.unlink(path, (err) => {
          if (err) {
            console.log(err)
          }
        })
        res.json(genre)
      })
      .catch((err) => console.log(err))
  }
)

module.exports = router
