const express = require("express")
const router = express.Router()
const favSong = require("../../../models/Favourite")
const fileUpload = require("express-fileupload")
router.use(fileUpload())

// @POST Method GET
// @url /api/user/fav/:user_id
// @desc Private Route

router.get("/:user_id", (req, res) => {
  favSong
    .find({ user_id: req.params.user_id })
    .sort({ date: -1 })
    .then((songs) => res.json(songs))
    .catch((err) => console.log(err))
})

// @POST Method POST
// @url /api/user/fav
// @desc Private Route
router.post("/", (req, res) => {
  const songObject = {}
  if (req.body.user_id) songObject.user_id = req.body.user_id
  if (req.body.title) songObject.title = req.body.title
  if (req.body.description) songObject.description = req.body.description
  if (req.body.song) songObject.song = req.body.song
  if (req.body.image) songObject.image = req.body.image
  songObject.imagePath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/Images/${req.body.image}`
  songObject.songPath = `http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/Songs/${req.body.song}`
  favSong
    .findOne({ song: songObject.song, user_id: songObject.user_id })
    .then((song) => {
      if (song) {
        console.log("Songify")
      } else {
        const newSong = new favSong(songObject)
        newSong
          .save()
          .then((song) => res.json(song))
          .catch((err) => console.log(err))
      }
    })
    .catch((err) => console.log(err))
})

// For Delete The Song
// @URL /api/user/fav/:user_id/:song
// @Method Post
router.delete("/:user_id/:song", (req, res) => {
  favSong
    .findOneAndRemove({ user_id: req.params.user_id, song: req.params.song })
    .then(() => res.json("Success"))
    .catch((err) => console.log(err))
})
module.exports = router
