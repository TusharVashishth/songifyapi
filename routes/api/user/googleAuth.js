const router = require("express").Router();
const passport = require("passport");
const GoogleKeys = require("../../../config/GoogleKeys");
const jwt = require("jsonwebtoken");
const User = require("../../../models/User");
const Key = require("../../../config/keys");
const GoogleStrategy = require("passport-google-oauth20").Strategy;

//Google Auth Passport Straties
passport.use(
  new GoogleStrategy(
    {
      clientID: GoogleKeys.google.clientID,
      clientSecret: GoogleKeys.google.clientSecret,
      callbackURL:
        "http://ec2-3-16-166-67.us-east-2.compute.amazonaws.com/auth/google/callback"
    },
    (accessToken, refreshToken, profile, cb) => {
      User.findOne({ authId: profile.id })
        .then((user, err) => {
          if (user) {
            console.log("Already exist");
          } else {
            const newUser = new User({
              authId: profile.id,
              name: profile.displayName,
              image: profile.photos[0].value
            });
            newUser
              .save()
              .then(() => console.log("SuccessFully Sign up"))
              .catch(err => console.log(err));
          }
          return cb(err, user);
        })
        .catch(() => {
          return cb(err, user);
        });
    }
  )
);
// Auth Login
router.get(
  "/auth/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

router.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    session: false
  }),
  function(req, res) {
    var user = req.user;

    const paylod = {
      id: user.authId,
      name: user.name
    };
    const secret = Key.secretKey;
    jwt.sign(paylod, secret, { expiresIn: "3600h" }, (err, token) => {
      if (token) {
        // console.log("Bearer " + token);
        res.json({
          success: true,
          token: "Bearer " + token
        });
      }
    });
    // console.log(user);
    // return res.json(user);
    // res.redirect("/");
  }
);

module.exports = router;
