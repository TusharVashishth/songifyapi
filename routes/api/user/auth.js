const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs'); 
const jwt = require('jsonwebtoken');
const User = require('../../../models/User');
const Key = require('../../../config/keys');
const loginValidation = require('../../../validation/loginValidation');
const registerValidation = require('../../../validation/registerValidation');
// @method POST
// @url /api/auth/register
// @url Public Url
router.post('/register', (req, res) => {
    const {isValid , errors} = registerValidation(req.body);
    if(!isValid) {
        return res.status(400).json(errors);
    }
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;

    User.findOne({ email: email })
        .then(user => {
            if (user) {
                return res.status(400).json({email:"Email already exist please choose another"})
            }
            const newUser = new User({
                name,email,password
            })
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser.save()
                        .then(user => res.json(user))
                        .catch(err => console.log(err))
                })
            })
        })
        .catch(err => console.log(err))
})

// @method POST
// @url /api/auth/login
// @url Public Url

router.post('/login', (req, res) => {
    const {isValid , errors} = loginValidation(req.body);
    if(!isValid) {
        return res.status(400).json(errors);
    }
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({ email: email })
        .then(user => {
            if (!user) {
                return res.status(404).json({email:"No Email Found"})
            }
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if (isMatch) {
                        const paylod = {
                            id: user.id,
                            name: user.name,
                            email:user.email
                        }
                        const secret = Key.secretKey;
                        jwt.sign(
                            paylod,
                            secret,
                            { expiresIn: '3600h' },
                            (err, token) => {
                                if (token) {
                                    res.json({
                                        success: true,
                                        token : "Bearer " + token
                                    })
                                }
                            }
                        )
                    }
                    else {
                        return res.status(400).json({password:"Password not match"})
                    }
                })
                .catch(err => console.log(err))
    })
})
module.exports = router;