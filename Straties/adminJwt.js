const Admin = require('../models/AdminAuth');
const Keys = require('../config/keys');
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
var opts = {}
opts.secretOrKey = Keys.secretKey;

module.exports = passport => {
    passport.use("adminStrategy",new JwtStrategy(opts, (jwt_payload, done) => {
        Admin.findOne({_id: jwt_payload.id}, (err, admin) => {
            if (err) {
                return done(err, false);
            }
            if (admin) {
                return done(null, admin);
            } else {
                return done(null, false);
                // or you could create a new account
            }
        });
    }));
}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();