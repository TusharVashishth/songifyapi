const User = require('../models/User');
const Keys = require('../config/keys');
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
var opts = {}
opts.secretOrKey = Keys.secretKey;

module.exports = passport => {
    passport.use("userStrategy", new JwtStrategy(opts, (jwt_payload, done) => {
        User.findOne({ _id: jwt_payload.id }, (err, user) => {
            if (err) {
                return done(err, false);
            }
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
                // or you could create a new account
            }
        });
    }));
}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();