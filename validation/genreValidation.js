const validator = require("validator")
const isEmpty = require("./isEmpty")

module.exports = function genreValidation(data) {
  let errors = {}
  data.type = !isEmpty(data.type) ? data.type : ""

  if (validator.isEmpty(data.type)) {
    errors.type = "Genre Type is Required"
  } else if (!validator.isLength(data.type, { min: 3, max: 35 })) {
    errors.type = "Type must be between 2 and 30 Charcter long"
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
