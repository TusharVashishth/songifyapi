const validator = require("validator")
const isEmpty = require("./isEmpty")

module.exports = function artistValidation(data) {
  let errors = {}
  data.name = !isEmpty(data.name) ? data.name : ""

  if (validator.isEmpty(data.name)) {
    errors.name = "Artist Name is Required"
  } else if (!validator.isLength(data.name, { min: 3, max: 35 })) {
    errors.name = "Name must be between 2 and 30 Charcter long"
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
