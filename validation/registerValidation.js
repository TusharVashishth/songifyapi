const validate = require('validator');
const isEmpty = require('./isEmpty');
module.exports = function registerValidation(data) {
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';

    if (validate.isEmpty(data.name)) {
         errors.name = "Name field is Required"
    }
    else if (validate.isAlpha(data.name, ['ar'])) {
         errors.name = "Only character Allowed"
    }
    else if (!validate.isLength(data.name, { min: 2, max: 30 })) {
        errors.name = "Name must be between 2 and 30 Charcter long";
    }

    if (validate.isEmpty(data.email)) {
         errors.email = "Email field is Required";
    }
    else if (!validate.isEmail(data.email)) {
         errors.email = "Please write Correct email"
    }
    if (validate.isEmpty(data.password)) {
         errors.password = "Password field is Required";
    }
    else if (!validate.isLength(data.password, { min: 6, max: 40 })) {
         errors.password = "Password must be 6 Chracter Long"
    }

    return {
        errors,
        isValid :isEmpty(errors)
    }

}