const validator = require("validator")
const isEmpty = require("./isEmpty")

module.exports = function adminDashboard(data) {
  let errors = {}
  data.heading = !isEmpty(data.heading) ? data.heading : ""
  data.text = !isEmpty(data.text) ? data.text : ""

  if (validator.isEmpty(data.heading)) {
    errors.heading = "Heading Feild is Required"
  } else if (!validator.isLength(data.heading, { min: 2, max: 30 })) {
    errors.heading = "Heading Must be between 2 and 30 Character Long"
  }

  if (validator.isEmpty(data.text)) {
    errors.text = "Text feild is Required"
  } else if (!validator.isLength(data.text, { min: 2, max: 150 })) {
    errors.text = "Text Must be between 2 and 150 Character Long"
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
