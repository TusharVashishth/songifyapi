const Validate = require("validator")
const isEmpty = require("./isEmpty")

module.exports = function songValidation(data) {
  let errors = {}
  data.title = !isEmpty(data.title) ? data.title : ""
  data.description = !isEmpty(data.description) ? data.description : ""
  data.length = !isEmpty(data.length) ? data.length : ""
  data.artist = !isEmpty(data.artist) ? data.artist : ""
  data.genre = !isEmpty(data.genre) ? data.genre : ""
  data.mood = !isEmpty(data.mood) ? data.mood : ""

  if (Validate.isEmpty(data.title)) {
    errors.title = "Title Field is required"
  } else if (Validate.isAlpha(data.title, ["ar"])) {
    errors.title = "Only Chracter Allowed"
  } else if (!Validate.isLength(data.title, { min: 3, max: 35 })) {
    errors.title = "Title must be between 2 and 30 Charcter long"
  }

  if (Validate.isEmpty(data.description)) {
    errors.description = "Description Field is required"
  } else if (Validate.isAlpha(data.description, ["ar"])) {
    errors.description = "Only Chracter Allowed"
  } else if (!Validate.isLength(data.description, { min: 3, max: 35 })) {
    errors.description = "Description must be between 2 and 30 Charcter long"
  }

  if (Validate.isEmpty(data.length)) {
    errors.length = "Length Field is required"
  }

  if (Validate.isEmpty(data.artist)) {
    errors.artist = "Artist Field is required"
  } else if (Validate.isAlpha(data.artist, ["ar"])) {
    errors.artist = "Only Character Allowed"
  } else if (!Validate.isLength(data.artist, { min: 3, max: 35 })) {
    errors.artist = "Artist Name must be between 2 and 30 Charcter long"
  }

  if (Validate.isEmpty(data.genre)) {
    errors.genre = "Genre Field is required"
  }
  if (Validate.isEmpty(data.mood)) {
    errors.mood = "Mood Field is required"
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
