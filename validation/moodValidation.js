const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function moodValidation(data) {
    let errors = {};
    data.type = !isEmpty(data.type) ? data.type : '';
    
    if (validator.isEmpty(data.type)) {
        errors.type = "Mood Type is Required";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }

}