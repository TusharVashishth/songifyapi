const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function loginValidation(data) {
    let errors = {};
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    
    if (validator.isEmpty(data.email)) {
        errors.email = "Email is Required";
    }
    else if (!validator.isEmail(data.email)) {
        errors.email = "Please Enter Valid Email";
    }

    if (validator.isEmpty(data.password)) {
        errors.password = "Password is Required";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }

}