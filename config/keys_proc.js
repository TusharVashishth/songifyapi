module.exports = {
    mongoURI: process.env.mongo_URI,
    secretKey: process.env.SECRET_KEY
}