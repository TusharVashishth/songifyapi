
if (process.env.NODE_ENV === 'production') {
    module.exports = require('./keys_proc');
}
else {
    module.exports = require('./keys_dev');
}