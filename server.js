const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");
const cors = require("cors");
// Body Parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Static Folder
process.env.PWD = process.cwd();
app.use(express.static(path.join(process.env.PWD, "public")));
// Requiring Files
const db = require("./config/keys").mongoURI;
const song = require("./routes/api/admin/songs");
const admin = require("./routes/api/admin/auth");
const user = require("./routes/api/user/auth");
const mood = require("./routes/api/admin/mood");
const artist = require("./routes/api/admin/artist");
const topArtist = require("./routes/api/admin/topArtist");
const recentSong = require("./routes/api/admin/recentSong");
const favSong = require("./routes/api/user/favouriteSongs");
const genre = require("./routes/api/admin/genre");
const adminDashboard = require("./routes/api/admin/admindashboard");
const authUsers = require("./routes/api/admin/users");
const GoogleAuth = require("./routes/api/user/googleAuth");

app.get("/", (req, res) => {
  res.send("Songify");
});

// For defining Routes
app.use("/api/admin/song", song);
app.use("/api/admin", admin);
app.use("/api/auth", user);
app.use("/api/admin/mood", mood);
app.use("/api/admin/add_artist", artist);
app.use("/api/admin/top_artist", topArtist);
app.use("/api/admin/recent", recentSong);
app.use("/api/user/fav", favSong);
app.use("/api/admin/genre", genre);
app.use("/api/admin/dashboard", adminDashboard);
app.use("/api/admin/auth-users", authUsers);
app.use(GoogleAuth);

// Passport Straties
app.use(passport.initialize());
require("./Straties/adminJwt")(passport);
require("./Straties/userJwt")(passport);
// For Mongo Db Connection
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => console.log("DataBase Connected Succesfully"))
  .catch(err => console.log(err));

// For Production
// if (process.env.NODE_ENV === "production") {
//   // Set Static file
//   app.use(express.static(__dirname, "client/build"))
//   // For Url
//   app.use("*", (req, res) => {
//     res.sendFile(path.resolve(__dirname, "client/build.index.html"))
//   })
// }

const PORT = process.env.PORT || 80 
app.listen(PORT, () => console.log(`Server is running on port ${PORT}`))
